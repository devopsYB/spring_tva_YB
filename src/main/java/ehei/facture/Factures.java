package ehei.facture;

public class Factures {

	private int id;
	private int Qte;
	private double Prix;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQte() {
		return Qte;
	}

	public void setQte(int qte) {
		Qte = qte;
	}

	public double getPrix() {
		return Prix;
	}

	public void setPrix(double prix) {
		Prix = prix;
	}
	
	public double getMontant() {
		return getPrix()*getQte();
	}
}
